// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const HttpClient = require("protractor-http-client").HttpClient
var HtmlReporter = require('protractor-beautiful-reporter');
var path = require('path');
const { SpecReporter } = require('jasmine-spec-reporter');
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    '../**/login.e2e-spec.ts',
    '../**/login.e2e-spec.2.ts'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'https://opensource-demo.orangehrmlive.com/index.php/auth/login',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    stopSpecOnExpectationFailure: true,
    print: function () { }
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    // require('jasmine-bail-fast');
    // jasmine.getEnv().bailFast();
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    jasmine.getEnv().addReporter(new HtmlReporter({
      baseDirectory: 'reports/screenshots',
      jsonsSubfolder: 'jsons',
      screenshotsSubfolder: 'images',
      docTitle: 'Demo',
      preserveDirectory: false,
      pathBuilder: function pathBuilder(spec, descriptions, results, capabilities) {
        return path.join(descriptions.join('-'));
      }
    }).getJasmine2Reporter());
    jasmine.getEnv().addReporter(
      new Jasmine2HtmlReporter({
        savePath: 'reports/PF'
      })
    );
  } 
};